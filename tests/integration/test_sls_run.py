import pathlib

from pytest_idem.runner import idem_cli

CACHE_DIR = ""


def test_sls_run(tmpdir, tests_dir):
    global CACHE_DIR
    CACHE_DIR = pathlib.Path(tmpdir)
    cmd_output = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run'}",
        "--params=params/run_params.sls",
        f"--esm-plugin=local",
        f"--cache-dir={CACHE_DIR / 'cache'}",
        "init",
    )
    ret = cmd_output["json"]
    # In test sls file we created 3 sls.run states. To verify all sls.run states, run 3 times.
    for i in range(1, 4):
        # there will be 2 states in each sls_run. to verify both, run 2 times
        for j in range(1, 3):
            tag = f"sls_|-service{i}:test_include_sls_run_{j}_|-test_|-service{i}:test_include_sls_run_{j}_|-test_include_sls_run_{j}_|-present"
            assert tag in ret
            assert ret[tag]["result"]
            resource = ret[tag]["new_state"]
            # this asserts that all the 3 sls.runs got resolved and all the included files are run and
            # params are different for each sls.run
            # this asserts that each sls.run got different set of params
            assert resource["key"] == f"key-{i}"
            assert resource["arg_bind"] == f"resource-{i}"
            # this asserts we can do argument binding  between resources in one sls.run
            assert resource["arg_bind_group"] == f"key-{i}"
            # this asserts that params passed from the command line are also taken into consideration
            assert resource["run_level_param"] == f"run_level_param-value-{j}"

        # assert nested sls.runs
        # Each sls.run has one nested module

        nested_tag = (
            f"sls_|-service{i}:service4_|-sls_|-service{i}:service4_|-service4_|-run"
        )
        assert nested_tag in ret

        nested_resource_tag = f"sls_|-service{i}:sls:service4:test_include_sls_run_3_|-test_|-service{i}:sls:service4:test_include_sls_run_3_|-test_include_sls_run_3_|-present"
        assert nested_resource_tag in ret
        assert ret[nested_resource_tag]["result"]
        nested_resource = ret[nested_resource_tag]["new_state"]

        assert nested_resource["key"] == f"key-{i}"
        assert nested_resource["arg_bind"] == f"resource-{i}"
        assert nested_resource["resource_id"] == "idem-test-3"

        nested_resource_tag_1 = f"sls_|-service{i}:sls:service4:test_include_sls_run_4_|-test_|-service{i}:sls:service4:test_include_sls_run_4_|-test_include_sls_run_4_|-present"
        assert nested_resource_tag_1 in ret
        assert ret[nested_resource_tag_1]["result"]
        nested_resource_1 = ret[nested_resource_tag_1]["new_state"]
        # This asserts we can refer values from resources in nested sls.runs also
        assert nested_resource_1["arg_bind_group"] == f"key-{i}"

    # This asserts the values referred from each sls.run
    assert "test_|-arg_bind_sls_run_|-arg_bind_sls_run_|-present" in ret
    assert ret["test_|-arg_bind_sls_run_|-arg_bind_sls_run_|-present"]["result"]

    assert (
        ret["test_|-arg_bind_sls_run_|-arg_bind_sls_run_|-present"]["new_state"][
            "resource_id"
        ]
        == "idem-test-1"
    )
    assert (
        ret["test_|-arg_bind_sls_run_|-arg_bind_sls_run_|-present"]["new_state"]["name"]
        == "key-1"
    )


def test_sls_run_from_esm(tests_dir):
    global CACHE_DIR
    # check if sls.run files can be referred from ESM if we do not run sls.run in this run
    cmd_output_esm = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run'}",
        "--params=params/run_params.sls",
        f"--cache-dir={CACHE_DIR / 'cache'}",
        f"--esm-plugin=local",
        "test_esm",
    )
    ret_esm = cmd_output_esm["json"]
    assert (
        "test_|-arg_bind_sls_run_test_esm_|-arg_bind_sls_run_test_esm_|-present"
        in ret_esm
    )
    esm_resource = ret_esm[
        "test_|-arg_bind_sls_run_test_esm_|-arg_bind_sls_run_test_esm_|-present"
    ]
    assert esm_resource["result"]
    assert esm_resource["new_state"]["resource_id"] == "idem-test-1"
    assert esm_resource["new_state"]["key"] == "key-1"


def test_invalid_sls_source_in_sls_run(tests_dir):
    """
    Verify that a proper error is shown if an invalid sls_source_path is provided in sls.run
    Only sls.run state should fail with proper error. Other states should execute successfully
    """
    cmd_output = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run'}",
        "test_invalid_sls_sources",
    )
    ret = cmd_output["json"]

    assert_invalid_sls_run(ret)

    # verify proper error is shown in comment
    assert (
        "Error in gathering sls_sources for sls.run service1"
        in ret["sls_|-service1_|-service1_|-run"]["comment"]
    )
    assert (
        "Error while collecting blocks: LookupError: Could not find SLS ref 'sls.invalid_file_path' in sources"
        in ret["sls_|-service1_|-service1_|-run"]["comment"]
    )


def test_invalid_params_files_in_sls_run(tests_dir):
    """
    Verify that a proper error is shown if an invalid params file path is provided in sls.run
    Only sls.run state should fail with proper error. Other states should execute successfully
    """
    cmd_output = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run'}",
        "test_invalid_params_file",
    )
    ret = cmd_output["json"]

    assert_invalid_sls_run(ret)

    # verify proper error is shown in comment
    assert (
        "Error in gathering params files for sls.run service1"
        in ret["sls_|-service1_|-service1_|-run"]["comment"]
    )
    assert (
        "Error while collecting blocks: LookupError: Could not find SLS ref 'params.invalid_params_file' in sources"
        in ret["sls_|-service1_|-service1_|-run"]["comment"]
    )


def test_duplicate_ids_in_sls_run(tests_dir):
    """
    Verify that a proper error is shown if an invalid params file path is provided in sls.run
    Only sls.run state should fail with proper error. Other states should execute successfully
    """
    cmd_output = idem_cli(
        "state",
        f"--tree={tests_dir / 'sls' / 'sls_run'}",
        "test_duplicate_ids",
    )
    ret = cmd_output["json"]

    assert_invalid_sls_run(ret)

    # verify proper error is shown in comment
    assert (
        "Error in gathering sls_sources for sls.run service1"
        in ret["sls_|-service1_|-service1_|-run"]["comment"]
    )
    assert (
        "Duplicate state declarations found in SLS tree: duplicate_id"
        in ret["sls_|-service1_|-service1_|-run"]["comment"]
    )


def assert_invalid_sls_run(ret):
    # verify that sls.run is not resolved and included files are not run
    assert len(ret) == 3

    # verify all states are rendered
    assert "test_|-multi_result_new_state_1_|-multi_result_new_state_1_|-present" in ret
    assert "test_|-first thing_|-first thing_|-succeed_with_changes" in ret
    assert "sls_|-service1_|-service1_|-run" in ret

    # verify that states other than sls.run is successful
    assert (
        ret["test_|-multi_result_new_state_1_|-multi_result_new_state_1_|-present"][
            "result"
        ]
        is True
    )
    assert (
        ret["test_|-first thing_|-first thing_|-succeed_with_changes"]["result"] is True
    )

    # verify sls.run is failed since we provided invalid path in sls_sources
    assert ret["sls_|-service1_|-service1_|-run"]["result"] is False
