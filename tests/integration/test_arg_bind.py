import pytest
from pytest_idem.runner import run_sls


def test_arg_bind_requisite():
    """
    Test that you can do arg_binding
    """
    ret = run_sls(["arg_bind"])
    assert ret["test_|-arg bind_|-arg bind_|-succeed_with_arg_bind"]["result"] is True
    changes = ret.get("test_|-arg bind_|-arg bind_|-succeed_with_arg_bind", {}).get(
        "changes", {}
    )
    assert (
        changes.get("testing", {}).get("test1", None) == "Something pretended to change"
    )
    assert (
        changes.get("testing", {}).get("test2", None) == "Something pretended to change"
    )

    assert (
        ret["test_|-indexed arg_|-indexed arg_|-succeed_with_arg_bind"]["result"]
        is True
    )
    changes = ret.get(
        "test_|-indexed arg_|-indexed arg_|-succeed_with_arg_bind", {}
    ).get("changes", {})
    assert changes.get("testing", {}).get("tests", [])[1].get("test", {}) == "new_test"

    assert (
        ret["test_|-fail no new_state_|-fail no new_state_|-succeed_with_arg_bind"][
            "result"
        ]
        is False
    )
    assert (
        ret["test_|-fail no new_state_|-fail no new_state_|-succeed_with_arg_bind"][
            "comment"
        ]
        == '"test:second thing" state does not have "new_state" in the state returns.'
    )

    assert (
        ret["test_|-fail arg not found_|-fail arg not found_|-succeed_with_arg_bind"][
            "result"
        ]
        is False
    )
    assert ret[
        "test_|-fail arg not found_|-fail arg not found_|-succeed_with_arg_bind"
    ]["comment"] == (
        '"Failed to parse "testing:arg_not_found" for state "test". Key "arg_not_found" is not found as part of the state "new_state".'
    )

    assert (
        ret[
            "test_|-fail referenced arg index not found_|-fail referenced arg index not found_|-succeed_with_arg_bind"
        ]["result"]
        is False
    )
    assert (
        ret[
            "test_|-fail referenced arg index not found_|-fail referenced arg index not found_|-succeed_with_arg_bind"
        ]["comment"]
        == '"Failed to parse "tests[0][5]:new" for state "test". Cannot parse argument value for key "tests" and index "5", because argument value is not a list or it does not include element with index "5".'
    )

    assert (
        ret[
            "test_|-fail arg index not found_|-fail arg index not found_|-succeed_with_arg_bind"
        ]["result"]
        is False
    )
    assert ret[
        "test_|-fail arg index not found_|-fail arg index not found_|-succeed_with_arg_bind"
    ]["comment"] == (
        'Cannot parse argument value for key "test" and index "1", because argument value is not a list or it does not include element with index "1".'
    )


def test_arg_bind_reference():
    """
    Test that you can do argument binding via references
    """
    ret = run_sls(["arg_bind_ref"])
    assert (
        ret[
            "test_|-arg_bind_new_state_collection_|-arg_bind_new_state_collection_|-succeed_with_arg_bind"
        ]["result"]
        is True
    )
    changes = ret.get(
        "test_|-arg_bind_new_state_collection_|-arg_bind_new_state_collection_|-succeed_with_arg_bind",
        {},
    ).get("changes", {})
    assert changes.get("testing", None) == ["s1", "s2"]
    assert (
        ret["test_|-arg_bind_wildcard_|-arg_bind_wildcard_|-succeed_with_arg_bind"][
            "result"
        ]
        is True
    )
    changes = ret.get(
        "test_|-arg_bind_wildcard_|-arg_bind_wildcard_|-succeed_with_arg_bind",
        {},
    ).get("changes", {})
    assert changes.get("testing", None) == ["s1", "s2"]

    assert (
        ret[
            "test_|-arg_bind_wildcard_error_|-arg_bind_wildcard_error_|-succeed_with_arg_bind"
        ]["result"]
        is False
    )
    comment = ret.get(
        "test_|-arg_bind_wildcard_error_|-arg_bind_wildcard_error_|-succeed_with_arg_bind",
        {},
    ).get("comment", "")
    assert (
        comment
        == '"Failed to parse "[*][*]" for state "test". Cannot parse argument value for key "[*][*]" for index "*", because argument key is not a list.'
    )

    assert (
        ret["test_|-arg bind ref_|-arg bind ref_|-succeed_with_arg_bind"]["result"]
        is True
    )
    changes = ret.get(
        "test_|-arg bind ref_|-arg bind ref_|-succeed_with_arg_bind", {}
    ).get("changes", {})
    assert (
        changes.get("testing", {}).get("test1", None)
        == "First - Something pretended to change. Second - new_test. Finished"
    )
    assert changes.get("testing", {}).get("test2", None) == "new_test"
    assert (
        changes.get("testing", {}).get("test3", [])[0]
        == "Something pretended to change"
    )
    assert changes.get("testing", {}).get("test3", [])[1] == "new_test -- new_test"
    assert changes.get("testing", {}).get("test4", None) == {
        "old": "Unchanged",
        "new": "Something pretended to change",
    }

    assert (
        ret[
            "test_|-reference within list_|-reference within list_|-succeed_with_arg_bind"
        ]["result"]
        is True
    ), ret["comment"]
    """
    assert (
        ret[
            "test_|-fail reference format_|-fail reference format_|-succeed_with_arg_bind"
        ]["comment"]
        == 'Cannot set argument value for index "1", because "test" is not a list or it does not include element with index "1".'
    )
    """

    assert (
        ret["test_|-arg bind dict key_|-arg bind dict key_|-succeed_with_arg_bind"][
            "result"
        ]
        is True
    )
    changes = ret.get(
        "test_|-arg bind dict key_|-arg bind dict key_|-succeed_with_arg_bind", {}
    ).get("changes", {})
    assert (
        changes.get("testing", {}).get("test1[0][1]", None)
        == "Something pretended to change"
    )


def test_arg_bind_in_jinja():
    """
    Test that you can do arg_binding inside jinja block
    """

    ret = run_sls(["arg_bind_in_jinja"])
    assert (
        ret["test_|-multi_result_new_state_|-multi_result_new_state_|-present"][
            "result"
        ]
        is True
    )

    new_state = ret.get(
        "test_|-multi_result_new_state_|-multi_result_new_state_|-present",
        {},
    ).get("new_state", {})

    expected_resource_ids = [v["resource_id"] for v in new_state.values()]

    assert (
        ret["trigger_|-test_arg_bind_in_jinja_|-test_arg_bind_in_jinja_|-build"][
            "result"
        ]
        is True
    )
    arg_binded_new_state = ret[
        "trigger_|-test_arg_bind_in_jinja_|-test_arg_bind_in_jinja_|-build"
    ]["new_state"]

    assert expected_resource_ids == arg_binded_new_state.get("triggers").get(
        "resource_ids"
    )
    assert "nested_value1" == arg_binded_new_state.get("triggers").get(
        "key_with_nested_value_0"
    )
    assert "nested_value2" == arg_binded_new_state.get("triggers").get(
        "key_with_nested_value_1"
    )
    assert "nested_value3" == arg_binded_new_state.get("triggers").get(
        "key_with_nested_value_2"
    )


def test_arg_bind_in_jinja_error():
    """
    Test arg_binding in jinja but with incorrect reference. Should throw error
    """

    with pytest.raises(ValueError) as exc:
        run_sls(["arg_bind_in_jinja_error"])
    assert (
        "Arg_bind template '${test:multi_result_new_state_1}' could not be resolved to any value"
        in exc.value.args[0]
    )


def test_include_delayed_rend():
    """
    Test if delayed rendering works in  included files
    """

    ret = run_sls(["include_delayed_rend"])

    # assert on the arg-bind value in second included file

    state_tag_1 = "test_|-succeed_with_changes_state_1_|-succeed_with_changes_state_1_|-succeed_with_changes"

    state_arg_binded_to_state_1_tag = (
        "test_|-test_arg_bind_jinja_1_|-test_arg_bind_jinja_1_|-succeed_with_comment"
    )

    assert ret[state_tag_1]["result"] is True

    new_state = ret.get(state_tag_1).get("new_state")

    arg_binded_value = ret.get(state_arg_binded_to_state_1_tag).get("comment")

    assert new_state
    assert arg_binded_value
    assert arg_binded_value == new_state["testing"]["old"]

    # assert on the arg-bind value in second included file

    state_tag_2 = "test_|-succeed_with_changes_state_2_|-succeed_with_changes_state_2_|-succeed_with_changes"

    state_arg_binded_to_state_2_tag = (
        "test_|-test_arg_bind_jinja_2_|-test_arg_bind_jinja_2_|-succeed_with_comment"
    )

    assert ret[state_tag_2]["result"] is True

    new_state_2 = ret.get(state_tag_2).get("new_state")

    arg_binded_value_2 = ret.get(state_arg_binded_to_state_2_tag).get("comment")

    assert new_state_2
    assert arg_binded_value_2
    assert arg_binded_value_2 == new_state_2["testing"]["new"]

    # assert on arg-binded value in third file

    state_arg_binded_to_state_2_tag = (
        "test_|-test_arg_bind_jinja_3_|-test_arg_bind_jinja_3_|-succeed_with_comment"
    )

    arg_binded_value_3 = ret.get(state_arg_binded_to_state_2_tag).get("comment")

    assert ret[state_arg_binded_to_state_2_tag]["result"] is True
    assert arg_binded_value_3
    assert arg_binded_value_3 == new_state["testing"]["old"]

    state_arg_binded_to_state_1_tag = (
        "test_|-test_arg_bind_jinja_4_|-test_arg_bind_jinja_4_|-succeed_with_comment"
    )

    arg_binded_value_4 = ret.get(state_arg_binded_to_state_1_tag).get("comment")

    assert ret[state_arg_binded_to_state_1_tag]["result"] is True
    assert arg_binded_value_4
    assert arg_binded_value_4 == new_state_2["testing"]["new"]


def test_arg_bind_ref_in_test_mode():
    ret = run_sls(["arg_bind_ref_test_mode"], test=True)
    assert (
        ret["test_|-arg bind ref_|-arg bind ref_|-succeed_with_arg_bind"]["result"]
        is True
    )
    changes = ret.get(
        "test_|-arg bind ref_|-arg bind ref_|-succeed_with_arg_bind", {}
    ).get("changes", {})
    assert (
        changes.get("testing", {}).get("test1", None)
        == "First - new_param_value_known_after_applying. Second - new_param_value_known_after_applying. Finished"
    )
    assert (
        changes.get("testing", {}).get("test2", None)
        == "new_param_value_known_after_applying"
    )
    assert len(changes.get("testing", {}).get("test3", [])) == 2
    assert "new_param_value_known_after_applying" in changes.get("testing", {}).get(
        "test3", []
    )
    assert (
        "new_param_value_known_after_applying -- new_param_value_known_after_applying"
        in changes.get("testing", {}).get("test3", [])
    )
