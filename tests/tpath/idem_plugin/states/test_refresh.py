__contracts__ = ["resource"]


def present(
    hub,
    ctx,
    name: str,
):
    """
    Return the previous old_state, if it's not specified in sls, and the given new_state.
    Raise an error on fail
    """
    ret = {
        "name": name,
        "old_state": None,
        "new_state": {"changed": "value"},
        "result": True,
        "comment": None,
    }
    return ret


def absent(
    hub,
    ctx,
    name: str,
):
    return {
        "name": name,
    }


async def describe(hub, ctx):
    return {"test": {"test_refresh.present": []}}
