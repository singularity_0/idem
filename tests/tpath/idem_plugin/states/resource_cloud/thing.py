def present(hub, ctx, name: str):
    ret = {
        "name": name,
        "changes": {},
        "result": True,
        "comment": [],
        "new_state": ctx.acct,
    }
    return ret
