test-output-empty-file-name:
    data.write:
       - file_name: ""
       - parameters:
            resource_name_1: resource_id_1

test-output-missing-template:
    data.write:
       - file_name: "result.json"
       - template_file_name: "jinja.template"
       - parameters:
            resource_name_1: resource_id_1

test-output-no-template:
    data.write:
       - file_name: "result.json"
       - parameters:
             resource_name_1: resource_id_1

test-output-with-template:
    data.write:
       - file_name: "result.json"
       - template: '{% raw %}
                        {% for key, value in parameters.items() %}
                            This object was created by test {{ key }} {{ value }}
                        {% endfor %}
                    {% endraw %}'
       - parameters:
            resource_name_1: resource_id_1
            resource_name_2: resource_id_2
            resource_name_3: resource_id_3
