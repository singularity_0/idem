succeed_with_changes_state_1:
  test.succeed_with_changes:
    - name: succeed_with_changes_state_1

#!require:succeed_with_changes_state_1

{% set nested_value = hub.idem.arg_bind.resolve("${test:succeed_with_changes_state_1:testing:old}") %}
test_arg_bind_jinja_1:
  test.succeed_with_comment:
    - comment: {{ nested_value }}
#!END
