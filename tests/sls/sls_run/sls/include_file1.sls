test_include_sls_run_1:
  test.present:
    - resource_id: idem-test-1
    - new_state:
        key: {{ params[ "key" ] }}
        arg_bind: {{ params[ "arg_bind" ] }}
        run_level_param: {{ params["run_level_param-1"] }}
        resource_id: idem-test-1
        arg_bind_group: ${test:test_include_sls_run_2:key}
        db_parameter_group_name:
          - ${test:test_include_sls_run_2:db_parameter_group_name}
          - ${test:test_include_sls_run_2:db_parameter_group_name}
    - test_arg:
        - ${test:test_include_sls_run_2:db_parameter_group_name}
    - result: true


service4:
  sls.run:
    - sls_sources:
      - sls.include_file3
    - params:
        - params.file{{ params.get("file_index", "3") }}
    - db_parameter_group_name: service3_db_parameter_group
