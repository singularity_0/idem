service1:
  sls.run:
    - sls_sources:
      - sls.include_file1
      - sls.include_file2
    - params:
        - params.file1
        - params.file_12
    - db_parameter_group_name: service1_db_parameter_group

service2:
  sls.run:
    - sls_sources:
      - sls.include_file1
      - sls.include_file2
    - params:
        - params.file2
    - db_parameter_group_name: service2_db_parameter_group

service3:
  sls.run:
    - sls_sources:
      - sls.include_file1
      - sls.include_file2
    - params:
        - params.file3
    - db_parameter_group_name: service3_db_parameter_group


first thing:
  test.succeed_with_changes


multi_result_new_state_1:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        "resource_id_1": "resource-1"
        "name": "subnet1"
        "resource_id_2": "resource-2"
        "resource_id_3": "resource-3"


arg_bind_sls_run:
  test.present:
    - result: True
    - changes: 'skip'
    - new_state:
        "resource_id": ${sls:service1:test:test_include_sls_run_1:resource_id}
        "name": ${sls:service1:test:test_include_sls_run_2:key}
